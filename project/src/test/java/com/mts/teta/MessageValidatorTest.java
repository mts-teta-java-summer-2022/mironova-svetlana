package com.mts.teta;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.mts.teta.model.Message;
import com.mts.teta.validator.MessageValidatorImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


public class MessageValidatorTest {

    private final MessageValidatorImpl validator = new MessageValidatorImpl();
    private static String validJSON;
    private static String invalidJSON;

    @BeforeAll
    static void setJSON() {
        validJSON = "{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"88005553535\"\n" +
                "}";
        invalidJSON = "{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\":\n" +
                "}";
    }

    @Test
    void testJSON() {
        Message validMessage = new Message(validJSON, Message.EnrichmentType.MSISDN);
        Message invalidMessage = new Message(invalidJSON, Message.EnrichmentType.MSISDN);

        assertTrue(validator.isValid(validMessage));
        assertFalse(validator.isValid(invalidMessage));
    }

}
