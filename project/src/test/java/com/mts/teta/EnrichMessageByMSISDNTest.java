package com.mts.teta;

import static org.junit.jupiter.api.Assertions.assertEquals;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mts.teta.api.repository.IMessageRepository;
import com.mts.teta.api.repository.IUserRepository;
import com.mts.teta.model.User;
import com.mts.teta.model.Message;
import com.mts.teta.repository.EnrichedMessageRepositoryImpl;
import com.mts.teta.repository.UnenrichedMessageRepositoryImpl;
import com.mts.teta.repository.UserRepositoryImpl;
import com.mts.teta.service.EnrichMessageByMSISDNImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class EnrichMessageByMSISDNTest {

    private final IMessageRepository enrichedMessageRepository = new EnrichedMessageRepositoryImpl();
    private final IMessageRepository unenrichedMessageRepository= new UnenrichedMessageRepositoryImpl();
    private static final IUserRepository userRepository = new UserRepositoryImpl();
    private final EnrichMessageByMSISDNImpl enrichValidator = new EnrichMessageByMSISDNImpl(userRepository, enrichedMessageRepository, unenrichedMessageRepository);
    private static final List<String> contentList = new ArrayList<>();
    private static final List<String> expectedContent = new ArrayList<>();

    @BeforeEach
    void clearContent() {
        enrichedMessageRepository.clear();
        unenrichedMessageRepository.clear();
    }

    @BeforeAll
    static void setContent() {
        contentList.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"88005553535\"\n" +
                "}");
        contentList.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+78007896565\"\n" +
                "}");
        contentList.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+79803537865\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Vasya\",\n" +
                "    \"lastName\": \"Ivanov\"\n" +
                "  }\n" +
                "}");
        contentList.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\"\n" +
                "}");
        contentList.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+798035865\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Vasya\",\n" +
                "    \"lastName\": \"Ivanov\"\n" +
                "  }\n" +
                "}");
        contentList.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Vasya\",\n" +
                "    \"lastName\": \"Ivanov\"\n" +
                "  }\n" +
                "}");
        contentList.add("{\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+79803537865\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Vasya\",\n" +
                "    \"lastName\": \"Ivanov\"\n" +
                "  }\n" +
                "}");
        contentList.add("{\n" +
                "  \"hi\": \"hi\",\n" +
                "  \"test\": \"test\",\n" +
                "  \"msisdn\": \"+79053760999\",\n" +
                "  \"enrichment\": {}\n" +
                "}");
        contentList.add("{\n" +
                "  \"hi\": \"hi\",\n" +
                "  \"test\": \"test\",\n" +
                "  \"msisdn\": \"88005553535\"\n" +
                "}");
        contentList.add("{\n" +
                "  \"hi\": \"hi\",\n" +
                "  \"test\": \"test\",\n" +
                "  \"msisdn\": \"+79053760999\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Vasya\",\n" +
                "    \"lastName\": \"Ivanov\"\n" +
                "  }\n" +
                "}");

        expectedContent.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"88005553535\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Test\",\n" +
                "    \"lastName\": \"User\"\n" +
                "  }\n" +
                "}");
        expectedContent.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+78007896565\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Iron\",\n" +
                "    \"lastName\": \"Man\"\n" +
                "  }\n" +
                "}");
        expectedContent.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+79803537865\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Ivan\",\n" +
                "    \"lastName\": \"Borisov\"\n" +
                "  }\n" +
                "}");
        expectedContent.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\"\n" +
                "}");
        expectedContent.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+798035865\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Vasya\",\n" +
                "    \"lastName\": \"Ivanov\"\n" +
                "  }\n" +
                "}");
        expectedContent.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Vasya\",\n" +
                "    \"lastName\": \"Ivanov\"\n" +
                "  }\n" +
                "}");
        expectedContent.add("{\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+79803537865\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Ivan\",\n" +
                "    \"lastName\": \"Borisov\"\n" +
                "  }\n" +
                "}");
        expectedContent.add("{\n" +
                "  \"hi\": \"hi\",\n" +
                "  \"test\": \"test\",\n" +
                "  \"msisdn\": \"+79053760999\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Sveta\",\n" +
                "    \"lastName\": \"Mironova\"\n" +
                "  }\n" +
                "}");
        expectedContent.add("{\n" +
                "  \"hi\": \"hi\",\n" +
                "  \"test\": \"test\",\n" +
                "  \"msisdn\": \"88005553535\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Test\",\n" +
                "    \"lastName\": \"User\"\n" +
                "  }\n" +
                "}");
        expectedContent.add("{\n" +
                "  \"hi\": \"hi\",\n" +
                "  \"test\": \"test\",\n" +
                "  \"msisdn\": \"+79053760999\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Sveta\",\n" +
                "    \"lastName\": \"Mironova\"\n" +
                "  }\n" +
                "}");

        userRepository.add("88005553535", new User("Test", "User"));
        userRepository.add("+78007896565", new User("Iron", "Man"));
        userRepository.add("+79803537865", new User("Ivan", "Borisov"));
        userRepository.add("+79053760999", new User("Sveta", "Mironova"));
    }

    @Test
    void testContent() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        List<Message> messages = new ArrayList<>();
        for (String content : contentList) {
            messages.add(new Message(content, Message.EnrichmentType.MSISDN));
        }
        for (int i = 0; i < messages.size(); i++) {
            enrichValidator.enrich(messages.get(i));
            assertEquals(mapper.readTree(expectedContent.get(i)), mapper.readTree(messages.get(i).getContent()));
        }
        assertEquals(7, enrichedMessageRepository.size());
        assertEquals(3, unenrichedMessageRepository.size());
    }

}
