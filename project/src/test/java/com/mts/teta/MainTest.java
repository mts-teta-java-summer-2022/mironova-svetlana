package com.mts.teta;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mts.teta.api.repository.IMessageRepository;
import com.mts.teta.api.repository.IUserRepository;
import com.mts.teta.model.User;
import com.mts.teta.model.Message;
import com.mts.teta.repository.EnrichedMessageRepositoryImpl;
import com.mts.teta.repository.UnenrichedMessageRepositoryImpl;
import com.mts.teta.repository.UserRepositoryImpl;
import com.mts.teta.service.EnrichMessageByMSISDNImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

public class MainTest {

    private final IMessageRepository enrichedMessageRepository = new EnrichedMessageRepositoryImpl();
    private final IMessageRepository unenrichedMessageRepository= new UnenrichedMessageRepositoryImpl();
    private static final IUserRepository userRepository = new UserRepositoryImpl();
    private final EnrichMessageByMSISDNImpl enrichValidator = new EnrichMessageByMSISDNImpl(userRepository, enrichedMessageRepository, unenrichedMessageRepository);

    private static final List<String> contentList = new ArrayList<>();
    private static final List<String> expectedContent = new ArrayList<>();
    private static final int THREAD_POOL_SIZE = 3;

    @BeforeAll
    static void setContent() {
        contentList.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"88005553535\"\n" +
                "}");
        contentList.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+78007896565\"\n" +
                "}");
        contentList.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+79803537865\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Vasya\",\n" +
                "    \"lastName\": \"Ivanov\"\n" +
                "  }\n" +
                "}");

        expectedContent.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"88005553535\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Test\",\n" +
                "    \"lastName\": \"User\"\n" +
                "  }\n" +
                "}");
        expectedContent.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+78007896565\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Iron\",\n" +
                "    \"lastName\": \"Man\"\n" +
                "  }\n" +
                "}");
        expectedContent.add("{\n" +
                "  \"action\": \"button_click\",\n" +
                "  \"page\": \"book_card\",\n" +
                "  \"msisdn\": \"+79803537865\",\n" +
                "  \"enrichment\": {\n" +
                "    \"firstName\": \"Ivan\",\n" +
                "    \"lastName\": \"Borisov\"\n" +
                "  }\n" +
                "}");

        userRepository.add("88005553535", new User("Test", "User"));
        userRepository.add("+78007896565", new User("Iron", "Man"));
        userRepository.add("+79803537865", new User("Ivan", "Borisov"));
    }

    @BeforeEach
    void clearContent() {
        enrichedMessageRepository.clear();
        unenrichedMessageRepository.clear();
    }

    @Test
    void sanityTest() {
        assertTrue(true);
    }

    @Test
    void testContentWithTreads() throws JsonProcessingException, ExecutionException, InterruptedException {
        List<Message> messages = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        ExecutorService executorService = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        for (String content : contentList) {
            messages.add(new Message(content, Message.EnrichmentType.MSISDN));
        }
        Future<String> firstEnrichContent = executorService.submit(() -> enrichValidator.enrich(messages.get(0)));
        Future<String> secondEnrichContent = executorService.submit(() -> enrichValidator.enrich(messages.get(1)));
        Future<String> thirdEnrichContent = executorService.submit(() -> enrichValidator.enrich(messages.get(2)));
        executorService.shutdown();

        String firstActualContent = firstEnrichContent.get();
        assertEquals(mapper.readTree(expectedContent.get(0)), mapper.readTree(firstActualContent));

        String secondActualContent = secondEnrichContent.get();
        assertEquals(mapper.readTree(expectedContent.get(1)), mapper.readTree(secondActualContent));

        String thirdActualContent = thirdEnrichContent.get();
        assertEquals(mapper.readTree(expectedContent.get(2)), mapper.readTree(thirdActualContent));
    }

}
