package com.mts.teta.util;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mts.teta.model.User;

public interface ValidationUtil {

    static boolean isEmpty(final String value) {
        return value == null || value.isEmpty();
    }

    static boolean isEmpty(final User user) {
        return user == null;
    }

    static boolean isEmpty(final JsonNode jsonString) {
        return jsonString == null;
    }

    static boolean isValid(final String content) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.readTree(content);
            return true;
        } catch (JacksonException e) {
            return false;
        }
    }

}
