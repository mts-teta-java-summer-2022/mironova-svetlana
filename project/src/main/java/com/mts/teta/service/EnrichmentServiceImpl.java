package com.mts.teta.service;

import com.mts.teta.api.repository.IMessageRepository;
import com.mts.teta.api.repository.IUserRepository;
import com.mts.teta.api.service.IEnrichmentService;
import com.mts.teta.api.service.IMessageValidator;
import com.mts.teta.model.Message;
import com.mts.teta.repository.EnrichedMessageRepositoryImpl;
import com.mts.teta.repository.UnenrichedMessageRepositoryImpl;
import com.mts.teta.repository.UserRepositoryImpl;

public class EnrichmentServiceImpl implements IEnrichmentService {

    private final IMessageValidator validator;

    public EnrichmentServiceImpl(IMessageValidator validator) {
        this.validator = validator;
    }

    @Override
    public String enrich(Message message) {
        if (!validator.isValid(message))
            return message.getContent();
        final IUserRepository userRepository = new UserRepositoryImpl();
        final IMessageRepository enrichedMessageRepository = new EnrichedMessageRepositoryImpl();
        final IMessageRepository unenrichedMessageRepository = new UnenrichedMessageRepositoryImpl();
        final Message.EnrichmentType enrichmentType = message.getEnrichmentType();
        switch (enrichmentType) {
            case MSISDN:
                return new EnrichMessageByMSISDNImpl(userRepository, enrichedMessageRepository, unenrichedMessageRepository).enrich(message);
            default:
                return message.getContent();
        }
    }

}
