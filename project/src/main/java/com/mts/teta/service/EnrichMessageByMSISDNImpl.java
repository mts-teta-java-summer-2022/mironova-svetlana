package com.mts.teta.service;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mts.teta.api.repository.IMessageRepository;
import com.mts.teta.api.repository.IUserRepository;
import com.mts.teta.api.service.IEnrichMessage;
import com.mts.teta.model.Message;
import com.mts.teta.model.User;

import java.util.Locale;

import static com.mts.teta.util.ValidationUtil.isEmpty;

public class EnrichMessageByMSISDNImpl implements IEnrichMessage {

    private final IUserRepository userRepository;
    private final IMessageRepository enrichedMessageRepository;
    private final IMessageRepository unenrichedMessageRepository;

    public EnrichMessageByMSISDNImpl(IUserRepository userRepository, IMessageRepository enrichedMessageRepository, IMessageRepository unenrichedMessageRepository) {
        this.userRepository = userRepository;
        this.enrichedMessageRepository = enrichedMessageRepository;
        this.unenrichedMessageRepository = unenrichedMessageRepository;
    }

    @Override
    public String enrich(Message message) {
        final ObjectMapper mapper = new ObjectMapper();
        final String jsonString = message.getContent();
        try {
            JsonNode rootNode = mapper.readValue(jsonString, JsonNode.class);
            final JsonNode msisdnJson = rootNode.get(Message.EnrichmentType.MSISDN.name().toLowerCase(Locale.ROOT));
            if (isEmpty(msisdnJson)) {
                unenrichedMessageRepository.add(message);
                return message.getContent();
            }
            final String msisdn = msisdnJson.asText();
            if (isEmpty(msisdn) || !msisdn.matches("(\\+*)\\d{11}")) {
                unenrichedMessageRepository.add(message);
                return message.getContent();
            }
            final User user = userRepository.findOneByMSISND(msisdn);
            if (isEmpty(user)) {
                unenrichedMessageRepository.add(message);
                return message.getContent();
            }
            final JsonNode enrichmentJSON = rootNode.get("enrichment");
            if (isEmpty(enrichmentJSON)) {
                final ObjectNode enrichmentObject = ((ObjectNode) rootNode).putObject("enrichment");
                enrichmentObject.put("firstName", user.getFirstName());
                enrichmentObject.put("lastName", user.getLastName());
            }
            else {
                ((ObjectNode) enrichmentJSON).put("firstName", user.getFirstName());
                ((ObjectNode) enrichmentJSON).put("lastName", user.getLastName());
            }
            message.setContent(rootNode.toPrettyString());
            enrichedMessageRepository.add(message);
            return message.getContent();
        } catch (JacksonException e) {
            return message.getContent();
        }
    }

}
