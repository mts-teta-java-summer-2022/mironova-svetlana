package com.mts.teta.repository;

import com.mts.teta.api.repository.IMessageRepository;
import com.mts.teta.model.Message;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class EnrichedMessageRepositoryImpl implements IMessageRepository {

    private final List<Message> enrichedMessages = new CopyOnWriteArrayList<>();

    @Override
    public void add(Message message) {
        enrichedMessages.add(message);
    }

    @Override
    public void clear() {
        enrichedMessages.clear();
    }

    @Override
    public int size() {
        return enrichedMessages.size();
    }

}
