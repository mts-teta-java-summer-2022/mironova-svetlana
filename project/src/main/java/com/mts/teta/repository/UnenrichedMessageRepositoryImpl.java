package com.mts.teta.repository;

import com.mts.teta.api.repository.IMessageRepository;
import com.mts.teta.model.Message;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class UnenrichedMessageRepositoryImpl implements IMessageRepository {

    private final List<Message> unenrichedMessages = new CopyOnWriteArrayList<>();

    @Override
    public void add(Message message) {
        unenrichedMessages.add(message);
    }

    @Override
    public void clear() {
        unenrichedMessages.clear();
    }

    @Override
    public int size() {
        return unenrichedMessages.size();
    }

}
