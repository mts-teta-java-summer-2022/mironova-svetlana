package com.mts.teta.repository;

import com.mts.teta.api.repository.IUserRepository;
import com.mts.teta.model.User;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class UserRepositoryImpl implements IUserRepository {

    private final Map<String, User> users = new ConcurrentHashMap<>();

    @Override
    public void add(String MSISDN, User user) {
        users.put(MSISDN, user);
    }

    @Override
    public void clear() {
        users.clear();
    }

    @Override
    public User findOneByMSISND(String MSISDN) {
        return users.get(MSISDN);
    }

}
