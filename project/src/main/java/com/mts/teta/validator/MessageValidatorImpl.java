package com.mts.teta.validator;

import com.mts.teta.api.service.IMessageValidator;
import com.mts.teta.model.Message;
import com.mts.teta.util.ValidationUtil;

import static com.mts.teta.util.ValidationUtil.isEmpty;

public class MessageValidatorImpl implements IMessageValidator {

    @Override
    public boolean isValid(Message message) {
        String content = message.getContent();
        if (isEmpty(content))
            return false;
        return ValidationUtil.isValid(content);
    }

}
