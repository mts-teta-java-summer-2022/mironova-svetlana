package com.mts.teta.api.service;

import com.mts.teta.model.Message;

public interface IEnrichMessage {

    String enrich(Message message);

}
