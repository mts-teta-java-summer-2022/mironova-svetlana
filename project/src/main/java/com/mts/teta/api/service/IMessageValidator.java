package com.mts.teta.api.service;

import com.mts.teta.model.Message;

public interface IMessageValidator {

    boolean isValid(Message message);

}
