package com.mts.teta.api.service;

import com.mts.teta.model.Message;

public interface IEnrichmentService {

    String enrich(Message message);

}
