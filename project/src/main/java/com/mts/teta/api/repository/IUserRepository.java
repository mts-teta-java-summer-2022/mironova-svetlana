package com.mts.teta.api.repository;

import com.mts.teta.model.User;

public interface IUserRepository {

    void add(String MSISDN, User user);

    void clear();

    User findOneByMSISND(String MSISDN);

}
