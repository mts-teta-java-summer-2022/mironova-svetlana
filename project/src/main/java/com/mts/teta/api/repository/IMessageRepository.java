package com.mts.teta.api.repository;

import com.mts.teta.model.Message;

public interface IMessageRepository {

    void add(Message message);

    void clear();

    int size();

}
