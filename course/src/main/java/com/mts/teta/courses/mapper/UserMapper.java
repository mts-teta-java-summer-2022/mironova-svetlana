package com.mts.teta.courses.mapper;

import com.mts.teta.courses.dto.UserDto;
import com.mts.teta.courses.dto.UserRequestDto;
import com.mts.teta.courses.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserDto toDto(User source);

    User toEntity(UserRequestDto source);

}
