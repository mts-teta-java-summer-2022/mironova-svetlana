package com.mts.teta.courses.service;

import com.mts.teta.courses.model.Course;
import com.mts.teta.courses.model.Lesson;
import com.mts.teta.courses.model.User;
import com.mts.teta.courses.repository.CourseRepository;
import com.mts.teta.courses.repository.LessonRepository;
import com.mts.teta.courses.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CourseService {

    private final CourseRepository courseRepository;
    private final LessonRepository lessonRepository;
    private final UserRepository userRepository;

    @Transactional(readOnly = true)
    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    @Transactional(readOnly = true)
    public Optional<Course> findById(Long id) {
        return courseRepository.findById(id);
    }

    @Transactional(readOnly = true)
    public List<Course> findByTitleWithPrefix(String titlePrefix) {
        return courseRepository.findByTitleStartsWith(titlePrefix);
    }

    @Transactional
    public Course create(Course course) {
        return courseRepository.save(course);
    }

    @Transactional
    public void update(Long id, String title, String author, String description) {
        Course course = courseRepository.findById(id).orElseThrow();
        course.setTitle(title);
        course.setAuthor(author);
        course.setDescription(description);
        courseRepository.save(course);
    }

    @Transactional
    public void delete(Long id) {
        Course course = courseRepository.findById(id).orElseThrow();
        courseRepository.delete(course);
    }

    @Transactional
    public Lesson createLesson(Long id, Lesson lesson) {
        Course course = courseRepository.findById(id).orElseThrow();
        lesson.setCourse(course);
        return lessonRepository.save(lesson);
    }

    @Transactional
    public void updateLesson(Long id, String title, String text) {
        Lesson lesson = lessonRepository.findById(id).orElseThrow();
        lesson.setTitle(title);
        lesson.setText(text);
        lessonRepository.save(lesson);
    }

    @Transactional
    public void deleteLesson(Long id) {
        Lesson lesson = lessonRepository.findById(id).orElseThrow();
        Course course = courseRepository.findById(lesson.getCourse().getId()).orElseThrow();
        course.getLessons().remove(lesson);
        lessonRepository.delete(lesson);
        courseRepository.save(course);
    }

    @Transactional
    public void subscribeUser(Long courseId, Long userId) {
        User user = userRepository.findById(userId).orElseThrow();
        Course course = courseRepository.findById(courseId).orElseThrow();
        course.getUsers().add(user);
        user.getCourses().add(course);
        courseRepository.save(course);
    }

    @Transactional
    public void unsubscribeUser(Long courseId, Long userId) {
        User user = userRepository.findById(userId).orElseThrow();
        Course course = courseRepository.findById(courseId).orElseThrow();
        user.getCourses().remove(course);
        course.getUsers().remove(user);
        courseRepository.save(course);
    }

}
