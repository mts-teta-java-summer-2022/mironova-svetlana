package com.mts.teta.courses.util;

import com.mts.teta.courses.enumerated.TitleCaseType;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;


@Documented
@Constraint(validatedBy = TitleCaseValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface TitleCase {

    TitleCaseType titleCaseType() default TitleCaseType.ANY;
    String message() default "Неккоректно введен заголовок {TitleCase}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

}
