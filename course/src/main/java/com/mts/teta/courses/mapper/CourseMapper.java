package com.mts.teta.courses.mapper;

import com.mts.teta.courses.dto.CourseCreateDto;
import com.mts.teta.courses.dto.CourseDto;
import com.mts.teta.courses.model.Course;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CourseMapper {

    CourseDto toDto(Course source);

    Course toEntity(CourseCreateDto source);

}
