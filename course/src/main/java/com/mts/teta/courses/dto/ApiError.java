package com.mts.teta.courses.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
public class ApiError {

    OffsetDateTime dateOccurred;
    String message;

}
