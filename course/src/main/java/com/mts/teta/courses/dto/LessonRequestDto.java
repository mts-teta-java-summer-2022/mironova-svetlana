package com.mts.teta.courses.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonRequestDto {

    @NotBlank(message = "Название урока является обязательным")
    private String title;
    @NotBlank(message = "Описание урока является обязательным")
    private String text;

}
