package com.mts.teta.courses.controller;

import com.mts.teta.courses.dto.*;
import com.mts.teta.courses.mapper.UserMapper;
import com.mts.teta.courses.model.User;
import com.mts.teta.courses.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
@Api
public class UserController {

    private final UserService userService;
    private final UserMapper userMapper;

    @GetMapping
    @ApiOperation("Получение списка всех пользователей")
    public ResponseEntity<List> findAll() {
        log.info("Получен запрос всех пользователей");
        return ResponseEntity.ok(userService.findAll().stream()
                .map(userMapper::toDto)
                .collect(toList()));
    }

    @GetMapping("/{id}")
    @ApiOperation("Получение пользователя по ID")
    public ResponseEntity<UserDto> findById(@PathVariable Long id) {
        log.info("Получен запрос пользователя [{}]", id);
        return ResponseEntity.of(userService.findById(id).map(userMapper::toDto));
    }

    @PostMapping
    @ApiOperation("Создание пользователя")
    public ResponseEntity<UserDto> create(@RequestBody @Valid UserRequestDto dto) {
        log.info("Получен запрос на создание пользователя");
        User user = userService.create(userMapper.toEntity(dto));
        return ResponseEntity.ok(userMapper.toDto(user));
    }

    @PostMapping("/{id}")
    @ApiOperation("Обновление пользователя")
    public void update(@PathVariable Long id, @RequestBody @Valid UserRequestDto dto) {
        log.info("Получен запрос на обновление пользователя [{}]", id);
        userService.update(id, dto.getUserName());
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Удаление пользователя")
    public void delete(@PathVariable Long id) {
        log.info("Получен запрос на удаление пользователя");
        userService.delete(id);
    }

}
