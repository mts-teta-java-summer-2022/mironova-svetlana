package com.mts.teta.courses.dto;

import com.mts.teta.courses.util.TitleCase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public  class CourseCreateDto {

    @TitleCase
    @NotBlank(message = "Название курса является обязательным")
    private String title;
    @NotBlank(message = "Автор курса является обязательным")
    private String author;
    @NotBlank(message = "Описание курса является обязательным")
    private String description;

}
