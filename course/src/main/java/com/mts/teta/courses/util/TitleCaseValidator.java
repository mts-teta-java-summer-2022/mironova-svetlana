package com.mts.teta.courses.util;

import com.mts.teta.courses.enumerated.TitleCaseType;
import net.logstash.logback.encoder.org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class TitleCaseValidator implements ConstraintValidator<TitleCase, String> {

    private TitleCaseType titleCaseType;

    public void setValidationType(TitleCaseType type){
        this.titleCaseType = type;
    }


    @Override
    public void initialize(TitleCase constraintAnnotation) {
        this.titleCaseType = constraintAnnotation.titleCaseType();
    }

    @Override
    public boolean isValid(String title, ConstraintValidatorContext constraintValidatorContext) {
        if (title.isBlank())
            return false;
        if (isAllValid(title)) {
            switch (titleCaseType) {
                case ANY:
                    return isAnyValid(title);
                case ENU:
                    return isEnuValid(title);
                case RU:
                    return isRuValid(title);
            }
        }
        return false;
    }

    private boolean isAllValid(String title) {
         return !(Pattern.compile("^[\"':,\\sa-zа-я]|\\s$|\\s{2,}|[\r\t\n]|" +
                "([A-Za-z]+.*[А-Яа-я]+.*|[А-Яа-я]+.*[A-Za-z]+.*)|[^A-Za-zА-Яа-я\\s\"':,]").matcher(title).find())
                 && StringUtils.countMatches(title, "'") % 2 == 0
                 && StringUtils.countMatches(title, "\"") % 2 == 0;
    }

    private boolean isAnyValid(String title) {
        if (Pattern.compile("[A-Za-z]").matcher(title).find())
            return isEnuValid(title);
        return isRuValid(title);
    }

    private boolean isEnuValid(String title) {
        String[] words = title.split(" ");
        List<String> specialWords = Arrays.asList("a", "but", "for", "or", "and", "not", "the", "an");
        for (String word : words) {
            if (!isFirstLetterUpperCase(word) && !specialWords.contains(word.toLowerCase())
                    || (isFirstLetterUpperCase(word) && specialWords.contains(word.toLowerCase())))
                return false;
        }
        return isFirstLetterUpperCase(words[0])
                && isFirstLetterUpperCase(words[words.length - 1]);
    }

    private boolean isRuValid(String title) {
        return isFirstLetterUpperCase(title);
    }

    private boolean isFirstLetterUpperCase(String title) {
        title = title.replaceAll("\"", "").replaceAll("'", "").replaceAll(",", "").replaceAll(":", "");
        return title.equals(title.substring(0, 1).toUpperCase() + title.substring(1).toLowerCase());
    }

}
