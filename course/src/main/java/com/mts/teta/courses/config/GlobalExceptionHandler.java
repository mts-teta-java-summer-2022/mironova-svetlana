package com.mts.teta.courses.config;

import com.mts.teta.courses.dto.ApiError;
import com.mts.teta.courses.dto.FieldValidationErrorDto;
import com.mts.teta.courses.dto.RequestValidationErrorDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.NoSuchElementException;

import static java.util.stream.Collectors.toList;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler
    public ResponseEntity noSuchElementExceptionHandler(NoSuchElementException e) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND)
                .body(new ApiError(OffsetDateTime.now(), e.getMessage()));
    }

    @ExceptionHandler
    public ResponseEntity methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        List<FieldValidationErrorDto> errors = e.getFieldErrors().stream()
                .map(error -> new FieldValidationErrorDto(error.getField(), error.getDefaultMessage()))
                .collect(toList());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                .body(new RequestValidationErrorDto(errors));
    }

}
