package com.mts.teta.courses.controller;

import com.mts.teta.courses.dto.*;
import com.mts.teta.courses.mapper.CourseMapper;
import com.mts.teta.courses.mapper.LessonMapper;
import com.mts.teta.courses.model.Course;
import com.mts.teta.courses.model.Lesson;
import com.mts.teta.courses.service.CourseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static java.util.Objects.requireNonNullElse;
import static java.util.stream.Collectors.toList;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/courses")
@Api
public class CourseController {

    private final CourseService courseService;
    private final CourseMapper courseMapper;
    private final LessonMapper lessonMapper;

    @GetMapping
    @ApiOperation("Получение списка всех курсов")
    public ResponseEntity<List> findAll() {
        log.info("Получен запрос всех курсов");
        return ResponseEntity.ok(courseService.findAll().stream()
                .map(courseMapper::toDto)
                .collect(toList()));
    }

    @GetMapping("/{id}")
    @ApiOperation("Получение курса по ID")
    public ResponseEntity<CourseDto> findById(@PathVariable Long id) {
        log.info("Получен запрос курса [{}]", id);
        return ResponseEntity.of(courseService.findById(id).map(courseMapper::toDto));
    }

    @GetMapping("/filter")
    @ApiOperation("Получение курса(ов) по префиксу")
    public ResponseEntity<List> getCoursesByTitlePrefix(@RequestParam(name = "titlePrefix", required = false) String titlePrefix) {
        log.info("Получен запрос курса с префиксом [{}]", titlePrefix);
        return ResponseEntity.ok(courseService.findByTitleWithPrefix(requireNonNullElse(titlePrefix, ""))
                .stream()
                .map(courseMapper::toDto)
                .collect(toList()));
    }

    @PostMapping
    @ApiOperation("Создание курса")
    public ResponseEntity<CourseDto> create(@RequestBody @Valid CourseCreateDto dto) {
        log.info("Получен запрос на создание курса");
        Course course = courseService.create(courseMapper.toEntity(dto));
        return ResponseEntity.ok(courseMapper.toDto(course));
    }

    @PostMapping("/{id}")
    @ApiOperation("Обновление курса")
    public void update(@PathVariable Long id, @RequestBody @Valid CourseUpdateDto dto) {
        log.info("Получен запрос на обновление курса [{}]", id);
        courseService.update(id, dto.getTitle(), dto.getAuthor(), dto.getDescription());
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Удаление курса")
    public void delete(@PathVariable Long id) {
        log.info("Получен запрос на удаление курса");
        courseService.delete(id);
    }

    @PostMapping("/{id}")
    @ApiOperation("Создание урока")
    public ResponseEntity<LessonDto> createLesson(@PathVariable("courseId") Long courseId, @RequestBody @Valid LessonRequestDto dto) {
        log.info("Получен запрос на создание урока");
        Lesson lesson = courseService.createLesson(courseId, lessonMapper.toEntity(dto));
        return ResponseEntity.ok(lessonMapper.toDto(lesson));
    }

    @PostMapping("/{id}")
    @ApiOperation("Обновление урока")
    public void updateLesson(@PathVariable Long id, @RequestBody @Valid LessonRequestDto dto) {
        log.info("Получен запрос на обновление урока [{}]", id);
        courseService.updateLesson(id, dto.getTitle(), dto.getText());
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Удаление урока")
    public void deleteLesson(@PathVariable Long id) {
        log.info("Получен запрос на удаление урока");
        courseService.deleteLesson(id);
    }

    @PostMapping("/{courseId}/subscribe")
    public void subscribeUser(@PathVariable("courseId") Long courseId, @RequestParam("userId") Long userId) {
        log.info("Получен запрос для оформления подписки пользователя на курс");
        courseService.subscribeUser(courseId, userId);
    }

    @PostMapping("/{courseId}/unsubscribe")
    public void unsubscribeUser(@PathVariable("courseId") Long courseId, @RequestParam("userId") Long userId) {
        log.info("Получен запрос для отписки пользователя от курса");
        courseService.unsubscribeUser(courseId, userId);
    }

}
