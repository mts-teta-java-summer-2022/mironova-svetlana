package com.mts.teta.courses.mapper;

import com.mts.teta.courses.dto.LessonDto;
import com.mts.teta.courses.dto.LessonRequestDto;
import com.mts.teta.courses.model.Lesson;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface LessonMapper {

    LessonDto toDto(Lesson source);

    Lesson toEntity(LessonRequestDto source);

}
