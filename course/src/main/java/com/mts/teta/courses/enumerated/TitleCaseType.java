package com.mts.teta.courses.enumerated;

public enum TitleCaseType {

    RU,
    ENU,
    ANY

}
