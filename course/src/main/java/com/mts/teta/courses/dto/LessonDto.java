package com.mts.teta.courses.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LessonDto {

    private Long id;
    private String title;
    private String text;
    private Long courseId;

}
