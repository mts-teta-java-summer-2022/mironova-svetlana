package com.mts.teta.courses;

import com.mts.teta.courses.enumerated.TitleCaseType;
import com.mts.teta.courses.util.TitleCaseValidator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TitleCaseValidatorTest {

    private final TitleCaseValidator validator = new TitleCaseValidator();

    @Test
    void isValid(String title) {
        validator.setValidationType(TitleCaseType.ANY);
        assertTrue(validator.isValid(title, null));
    }

    @Test
    void isValidEnu(String title) {
        validator.setValidationType(TitleCaseType.ENU);
        assertTrue(validator.isValid(title, null));
    }

    @Test
    void isValidRu(String title) {
        validator.setValidationType(TitleCaseType.RU);
        assertTrue(validator.isValid(title, null));
    }

    @Test
    void isNotValid(String title) {
        validator.setValidationType(TitleCaseType.ANY);
        assertFalse(validator.isValid(title, null));
    }

    @Test
    void isNotValidEnu(String title) {
        validator.setValidationType(TitleCaseType.ENU);
        assertFalse(validator.isValid(title, null));
    }

    @Test
    void isNotValidRu(String title) {
        validator.setValidationType(TitleCaseType.RU);
        assertFalse(validator.isValid(title, null));
    }

    @Test
    void testValid() {
        isValid("Основы кройки и шитья");
        isNotValid("Некорректно mix words");
        isNotValid("Некорректно из-за \n переноса строки");
        isNotValid("Спец \r символ");
        isNotValid("Очень  много    пробелов");
        isNotValid(" Начинаем с пробела");
        isNotValid("Какие178то лишние% сим?;$#волы");
        isNotValid("Заканчивается пробелом ");
        isValidEnu("Java Middle Developer");
        isValidEnu("Spring and Spring Boot");
        isNotValidEnu("Spring Or Spring");
        isNotValidEnu("Live or not live... That is the question");
        isValidRu("Корректное название");
        isValidRu("Проектирование дома 'будущего'");
        isValidRu("Введение в архитектурный дизайн");
        isNotValidRu("НЕКОрректное название");
        isNotValidRu("Некорректное \t Название");
        isNotValidRu("Архитектура И Дизайн");
    }

}
